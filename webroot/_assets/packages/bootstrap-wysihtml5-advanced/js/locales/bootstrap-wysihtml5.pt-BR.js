/**
 * Brazilian portuguese translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["bg-BG"] = {
        font_styles: {
            normal: "Texto Normal",
            h1: "Título 1",
            h2: "Título 2",
            h3: "Título 3",
            h4: "Título 4",
            h5: "Título 5",
            h6: "Título 6"
        },
        text: {
            alignLeft: "Alinhar a Esquerda",
            alignCenter: "Centralizar",
            alignJustify: "Justificado",
            alignRight: "Alinhar a Direita",
            blockquote: "Citação"
        },
        emphasis: {
            bold: "Negrito",
            italic: "Itálico",
            underline: "Sublinhado"
        },
        lists: {
            unordered: "Lista",
            ordered: "Lista Ordenada",
            outdent: "Recuar",
            indent: "Indentar"
        },
        link: {
            insert: "Inserir link",
            cancel: "Cancelar",
            target: "Target",
            invalid: "Inválido"
        },
        table: {
            insert: "Inserir Tabela",
            cancel: "Cancelar",
            rows: "Linhas",
            columns: "Колони",
            heading: {
                label: "Водеща част",
                none: "Без",
                row: "Първи ред",
                column: "Първа колона"
            },
            invalid: "Невалиден брой редове/колони"
        },
        image: {
            insert: "Вмъкни картинка",
            cancel: "Отмени",
            fromUrl: "От адрес",
            fromComputer: "От локален файл",
            invalid: "Невалиден адрес на изображение",
            error: "Възникна грешка при четенето на файла"
        },
        video: {
            insert: "Вмъкване на клип от YouTube",
            cancel: "Отмени",
            widescreen: "Широкоекранен формат",
            tv: "Телевизионен формат",
            invalid: "Невалиден адрес на видео"
        },
        html: {
            edit: "Редакртирай HTML"
        },
        colours: {
            black: "Черен",
            silver: "Сребърен",
            gray: "Сив",
            maroon: "Кестеняв",
            red: "Червен",
            purple: "Виолетов",
            green: "Зелен",
            olive: "Маслинен",
            navy: "Морско син",
            blue: "Син",
            orange: "Оранжев"
        }
    };
}(jQuery));
