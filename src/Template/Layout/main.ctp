<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $this->fetch('title') ?></title>

		<?= $this->element('resources') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('main_scripts') ?>
	</head>
	<body>
		<div id="wrapper">
			<?= $this->element('header') ?>
			<div id="page-wrapper">
				<?= $this->fetch('content') ?>
			</div>
			<?= $this->element('footer') ?>
			<?= $this->fetch('script') ?>
		</div>
	</body>
</html>
