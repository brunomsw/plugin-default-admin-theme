<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $this->fetch('title') ?></title>

		<?= $this->element('resources') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('main_css') ?>
		<?= $this->fetch('main_scripts') ?>
	</head>
	<body>
    <div class="container">
		    <?= $this->fetch('content') ?>
    </div>
    <?= $this->fetch('script') ?>
	</body>
</html>
