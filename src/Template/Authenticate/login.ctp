<fieldset style="margin: 115px auto 0 auto; max-width: 450px">
	<?php
		echo $this->Flash->render('auth');
		echo $this->Form->create();
		echo $this->Html->tag('legend', 'Acesso ao Painel');
		echo $this->Form->input('username', ['label' => 'Login', 'autofocus' => true]);
		echo $this->Form->input('password', ['label' => 'Senha', 'value' => '']);
		echo $this->Form->submit('Entrar', ['class' => 'btn btn-primary']);
		echo $this->Form->end();
	?>
</fieldset>
