<div class="alert alert-warning alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?= $message ?>
	<?php if(isset($text) and isset($url)) echo $this->Html->link($text, $url, ['class' => 'alert-link']) ?>
</div>