<?php

// scripts and stylesheets
$this->Html->script(
	[
	   '/theme/DefaultAdminTheme/packages/jquery/jquery-1.11.1.min',
	], ['block' => 'main_scripts']);
// CSS
$this->Html->css(
	[
    '/theme/DefaultAdminTheme/packages/bootstrap/css/bootstrap.min',
    '/theme/DefaultAdminTheme/packages/bootstrap-wysihtml5-advanced/css/bootstrap3-wysihtml5.min',
    '/theme/DefaultAdminTheme/packages/bootstrap-wysihtml5-advanced/css/bootstrap3-wysihtml5-editor.min',
    '/theme/DefaultAdminTheme/css/main',
    '/theme/DefaultAdminTheme/packages/font-awesome/css/font-awesome.min'
	], ['block' => true]);

$this->Html->script(
	[
	  '/theme/DefaultAdminTheme/packages/bootstrap/js/bootstrap.min',
	  '/theme/DefaultAdminTheme/packages/metis-menu/metisMenu.min',
    '/theme/DefaultAdminTheme/packages/bootstrap-wysihtml5-advanced/js/wysihtml5x-toolbar.min',
    '/theme/DefaultAdminTheme/packages/bootstrap-wysihtml5-advanced/js/bootstrap3-wysihtml5',
  	// '/packages/morris/raphael.min',
  	// '/packages/morris/morris.min',
	  // '/packages/morris/morris-data',
  	'/theme/DefaultAdminTheme/js/custom',

	], ['block' => true]);
?>
