<?php
namespace DefaultAdminTheme\View\Helper;

use ControlPanel\View\Helper\PanelMenuHelper as BasePanelMenuHelper;
use Cake\View\View;

/**
 * PanelMenu helper
 */
class PanelMenuHelper extends BasePanelMenuHelper {

	

	public function createMenuItem($title, $url, $icon, $isActive = false){
		$text = $this->_createText($title, $icon);
		$link = $this->Html->link($text, $url, ($isActive)? ['class' => 'active']: ['escape' => false]);
		return $this->Html->tag('li', $link);
	}

	public function createSecondLevelMenuTree($title, $icon, $itemsData){
		$text = $this->_createSecondLevelText($title, $icon);
		$link = $this->Html->link($text, '#', ['escape' => false]);
		$items = '';
		foreach($itemsData as $item)
			$items .= $this->createMenuItem($item['title'], $item['url'], $item['icon']);
		$ul = $this->Html->tag('ul', $items, ['class' => 'nav nav-second-level']);

		return $this->Html->tag('li', $link . $ul);
	}

	public function _createIcon($icon){
		return $this->Html->tag('i', '', ['class' => "fa fa-{$icon} fa-fw"]);
	}

	public function _createText($title, $icon){
		return $this->_createIcon($icon) . ' ' . $title;
	}

	public function _createSecondLevelText($title, $icon){
		return $this->_createText($title, $icon) . $this->Html->tag('span', null, ['class' => 'fa arrow']);
	}
}
